FROM golang:1.15.0

ADD   . /app/k8s-ci

WORKDIR /app/k8s-ci

RUN go build -o app_server main.go

EXPOSE 8888


CMD ["./app_server"]